FROM golang:1.8-wheezy

MAINTAINER PiLights Team <contact@pilights.de>

RUN apt-get update

RUN apt-get install -y --force-yes build-essential automake make gcc zip unzip libtool

RUN mkdir -pv /tmp/src && \
    cd /tmp/src && \
    wget https://github.com/google/protobuf/releases/download/v3.3.0/protoc-3.3.0-linux-x86_64.zip && \
    unzip protoc-3.3.0-linux-x86_64.zip && \
    cp bin/protoc /usr/bin && \

    GOPATH=/go && \
    mkdir -pv $GOPATH && \
    GOPATH=$GOPATH go get -u github.com/golang/protobuf/protoc-gen-go && \
    cp /go/bin/protoc-gen-go /usr/bin/ && \
    rm -rf $GOPATH
